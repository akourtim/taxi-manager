package com.taxi.manager.backend.dto;

import java.util.List;

/**
 * @author AKOURTIM Ahmed on 2019-05-11
 */
public class ProfileDTO {

    private Long id;

    private String name;

    private List<AuthorityDTO> listAuthorities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<AuthorityDTO> getListAuthorities() {
        return listAuthorities;
    }

    public void setListAuthorities(List<AuthorityDTO> listAuthorities) {
        this.listAuthorities = listAuthorities;
    }
}
