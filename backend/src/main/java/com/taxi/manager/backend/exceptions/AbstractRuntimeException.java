package com.taxi.manager.backend.exceptions;


/**
 * @author AKOURTIM Ahmed on 2019-05-11
 */
public class AbstractRuntimeException extends RuntimeException {

    private String code;

    AbstractRuntimeException(String messgae,String code){

        super(messgae);
        this.code=code;
    }

    AbstractRuntimeException(String message,Throwable cause,String code){

        super(message, cause);
        this.code=code;
    }

    String getCode(){
        return code;
    }
}
