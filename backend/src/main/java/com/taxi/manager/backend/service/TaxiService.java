package com.taxi.manager.backend.service;

import com.taxi.manager.backend.domain.Taxi;
import com.taxi.manager.backend.repositories.TaxiRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * @author AKOURTIM Ahmed on 2019-05-26
 */

@Service
@Transactional
public class TaxiService {

    @Autowired
    private TaxiRepository taxiRepository;

    public List<Taxi> findAll() {
        return taxiRepository.findAll();
    }


    public void addTaxi(Taxi taxi){


        taxi.setCreationDate(LocalDateTime.now());
        taxi.setModificationDate(LocalDateTime.now());
        taxi.setCreatedByUser("Ahmed");
        taxiRepository.save(taxi);
    }

}
