package com.taxi.manager.backend.repositories;

import com.taxi.manager.backend.domain.Taxi;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author AKOURTIM Ahmed on 2019-05-26
 */

@Repository
public interface TaxiRepository extends JpaRepository<Taxi,Long> {


    List<Taxi> findAll();



}
