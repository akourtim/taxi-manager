package com.taxi.manager.backend.dto;

/**
 * @author AKOURTIM Ahmed on 2019-05-12
 */
public class AuthorityDTO {


    private Long id;

    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
