export class Taxi {

  id: bigint;
  model: string;
  description: string;
  creationDate: object;
  modificationDate: object;
  createdBy: string;

}
