import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TaxiComponentComponent} from './taxi/taxi-component/taxi-component.component';
import {RouterModule, Routes} from '@angular/router';
import {TaxiFormComponent} from './taxi/taxi-form/taxi-form.component';




const routes: Routes = [
  { path: 'taxis', component: TaxiComponentComponent },
  { path: 'addTaxi', component: TaxiFormComponent }
];



@NgModule({
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
