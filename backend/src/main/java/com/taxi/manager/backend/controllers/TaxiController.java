package com.taxi.manager.backend.controllers;

import com.taxi.manager.backend.domain.Taxi;
import com.taxi.manager.backend.repositories.TaxiRepository;
import com.taxi.manager.backend.service.TaxiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author AKOURTIM Ahmed on 2019-05-26
 */

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class TaxiController {


    @Autowired
    private TaxiService taxiService;


    @GetMapping(value = "/taxis")
    public List<Taxi> getAllTaxis() {


        return taxiService.findAll();

    }


    @PostMapping(value="/taxis")
    public void addTaxi(@RequestBody Taxi taxi){

         taxiService.addTaxi(taxi);

    }
}
