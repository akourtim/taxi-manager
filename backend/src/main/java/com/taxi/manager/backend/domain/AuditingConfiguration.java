package com.taxi.manager.backend.domain;


import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableAsync;


/**
 * @author AKOURTIM Ahmed on 2019-05-11
 */
@EnableAsync
@EnableJpaAuditing
@SpringBootApplication
public class AuditingConfiguration {
}
