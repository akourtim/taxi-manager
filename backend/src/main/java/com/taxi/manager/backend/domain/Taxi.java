package com.taxi.manager.backend.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

/**
 * @author AKOURTIM Ahmed on 2019-05-12
 */

@Entity
public class Taxi extends AbstractEntity  implements Serializable {

    @Column(name ="model")
    private String model;
    
    @Column(name ="description")
    private String description;

    @Column(name ="service_begin_date")
    private Date serviceBeginDate;

    @Column(name ="updated_km")
    private Long updatedKm;

    @Column(name ="last_control_date")
    private Date lastControlDate;





    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getServiceBeginDate() {
        return serviceBeginDate;
    }

    public void setServiceBeginDate(Date serviceBeginDate) {
        this.serviceBeginDate = serviceBeginDate;
    }

    public Long getUpdatedKm() {
        return updatedKm;
    }

    public void setUpdatedKm(Long updatedKm) {
        this.updatedKm = updatedKm;
    }

    public Date getLastControlDate() {
        return lastControlDate;
    }

    public void setLastControlDate(Date lastControlDate) {
        this.lastControlDate = lastControlDate;
    }
}
