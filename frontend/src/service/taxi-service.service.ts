import {Injectable} from '@angular/core';
import {Taxi} from '../model/taxi';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class TaxiServiceService {

  private taxiUrl: string;

  constructor(private http: HttpClient) {

    this.taxiUrl = 'http://localhost:8080/taxis';
  }

  public findAllTaxis(): Observable<Taxi[]> {

    return this.http.get<Taxi[]>(this.taxiUrl);


  }

  public save(taxi: Taxi) {

    console.log(taxi.model);
    return this.http.post<Taxi>(this.taxiUrl, taxi);

  }
}
