package com.taxi.manager.backend.domain;

import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * @author AKOURTIM Ahmed on 2019-05-12
 */

@Entity

public class Bill extends AbstractEntity {

    private String reference;

    private String amount;

    private LocalDateTime billingDate;

    private Ride ride;


    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public LocalDateTime getBillingDate() {
        return billingDate;
    }

    public void setBillingDate(LocalDateTime billingDate) {
        this.billingDate = billingDate;
    }

    public Ride getRide() {
        return ride;
    }

    public void setRide(Ride ride) {
        this.ride = ride;
    }
}
