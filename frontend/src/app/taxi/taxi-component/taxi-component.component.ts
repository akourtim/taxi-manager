import { Component, OnInit } from '@angular/core';
import {Taxi} from '../../../model/taxi';
import {TaxiServiceService} from '../../../service/taxi-service.service';

@Component({
  selector: 'app-taxi-component',
  templateUrl: './taxi-component.component.html',
  styleUrls: ['./taxi-component.component.css']
})
export class TaxiComponentComponent implements OnInit {

  taxis: Taxi[];

  constructor(private taxiServiceService: TaxiServiceService) { }

  ngOnInit() {

    this.taxiServiceService.findAllTaxis().subscribe(data => {
      this.taxis = data;
    });
  }

}
