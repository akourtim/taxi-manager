import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { TaxiComponentComponent } from './taxi/taxi-component/taxi-component.component';
import { AppRoutingModule } from './app-routing.module';
import {TaxiServiceService} from '../service/taxi-service.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import { TaxiFormComponent } from './taxi/taxi-form/taxi-form.component';


@NgModule({
  declarations: [
    AppComponent,
    TaxiComponentComponent,
    TaxiFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [TaxiServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
