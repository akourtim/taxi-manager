import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaxiComponentComponent } from './taxi-component.component';

describe('TaxiComponentComponent', () => {
  let component: TaxiComponentComponent;
  let fixture: ComponentFixture<TaxiComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TaxiComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaxiComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
