package com.taxi.manager.backend.domain;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * @author AKOURTIM Ahmed on 2019-05-12
 */

@Entity
public class Customer extends AbstractEntity {

    private String fullName;

    private String mail;


    private String tel;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            },
            mappedBy = "listCustomer")
    private List<Ride> listRides;


    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public List<Ride> getListRides() {
        return listRides;
    }

    public void setListRides(List<Ride> listRides) {
        this.listRides = listRides;
    }
}
