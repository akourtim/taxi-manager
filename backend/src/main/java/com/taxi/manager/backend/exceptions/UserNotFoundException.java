package com.taxi.manager.backend.exceptions;


/**
 * @author AKOURTIM Ahmed on 2019-05-11
 */
public class UserNotFoundException extends AbstractRuntimeException {

    private static final String USER_NOT_FOUND = "USER_NOT_FOUND";

    UserNotFoundException(String messgae) {
        super(messgae, USER_NOT_FOUND);
    }
}
